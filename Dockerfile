FROM java:8-jdk

COPY ./server.yml /server.yml
COPY ./target/java-docker-opentracing-poc-publisher-1.0-SNAPSHOT.jar /java-docker-opentracing-poc-publisher-1.0-SNAPSHOT.jar

EXPOSE 8082

ENTRYPOINT ["java", "-jar", "java-docker-opentracing-poc-publisher-1.0-SNAPSHOT.jar", "server", "server.yml"]
